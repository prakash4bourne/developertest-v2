sap.ui.define([
		"au/com/bournedigital/developertestv2/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("au.com.bournedigital.developertestv2.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);