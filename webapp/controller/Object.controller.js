/*global location*/
sap.ui.define([
		"au/com/bournedigital/developertestv2/controller/BaseController",
		"sap/ui/model/json/JSONModel",
        "sap/ui/core/routing/History",
        "sap/ui/model/Filter",
        "sap/ui/model/FilterOperator",
		"au/com/bournedigital/developertestv2/model/formatter"
	], function (
		BaseController,
		JSONModel,
        History,
        Filter,
        FilterOperator,
		formatter
	) {
		"use strict";

		return BaseController.extend("au.com.bournedigital.developertestv2.controller.Object", {

			formatter: formatter,

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			/**
			 * Called when the worklist controller is instantiated.
			 * @public
			 */
			onInit : function () {
				// Model used to manipulate control states. The chosen values make sure,
				// detail page is busy indication immediately so there is no break in
				// between the busy indication for loading the view's meta data
				var iOriginalBusyDelay,
					oViewModel = new JSONModel({
						busy : true,
						delay : 0
					});

				this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

				// Store original busy indicator delay, so it can be restored later on
				iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
				this.setModel(oViewModel, "objectView");
				this.getOwnerComponent().getModel().metadataLoaded().then(function () {
						// Restore original busy indicator delay for the object view
						oViewModel.setProperty("/delay", iOriginalBusyDelay);
					}
                );
                this._applyStyle("objectHeader11","boldText");
                this._applyStyle("objectHeader12","boldText");
                this._applyStyle("objectHeader13","boldText");
                this._applyStyle("objectHeader21","boldText");
                this._applyStyle("objectHeader22","boldText");
                this._applyStyle("objectHeader23","boldText");
			},

			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * Event handler when the share in JAM button has been clicked
			 * @public
			 */
			onShareInJamPress : function () {
				var oViewModel = this.getModel("objectView"),
					oShareDialog = sap.ui.getCore().createComponent({
						name: "sap.collaboration.components.fiori.sharing.dialog",
						settings: {
							object:{
								id: location.href,
								share: oViewModel.getProperty("/shareOnJamTitle")
							}
						}
					});
				oShareDialog.open();
			},


			/* =========================================================== */
			/* internal methods                                            */
			/* =========================================================== */

			/**
			 * Binds the view to the object path.
			 * @function
			 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
			 * @private
			 */
			_onObjectMatched : function (oEvent) {
				var sObjectId =  oEvent.getParameter("arguments").objectId;
				this.getModel().metadataLoaded().then( function() {
					var sObjectPath = this.getModel().createKey("Orders", {
						OrderID :  sObjectId
					});
					this._bindView("/" + sObjectPath);
				}.bind(this));
			},

			/**
			 * Binds the view to the object path.
			 * @function
			 * @param {string} sObjectPath path to the object to be bound
			 * @private
			 */
			_bindView : function (sObjectPath) {
				var oViewModel = this.getModel("objectView"),
					oDataModel = this.getModel();

				this.getView().bindElement({
                    path: sObjectPath,
                    parameters:{ "expand" : "Customer"},
					events: {
						change: this._onBindingChange.bind(this),
						dataRequested: function () {
							oDataModel.metadataLoaded().then(function () {
								// Busy indicator on view should only be set if metadata is loaded,
								// otherwise there may be two busy indications next to each other on the
								// screen. This happens because route matched handler already calls '_bindView'
								// while metadata is loaded.
								oViewModel.setProperty("/busy", true);
							});
						},
						dataReceived: function () {
							oViewModel.setProperty("/busy", false);
						}
					}
				});
			},

			_onBindingChange : function () {
				var oView = this.getView(),
					oViewModel = this.getModel("objectView"),
					oElementBinding = oView.getElementBinding();

				// No data for the binding
				if (!oElementBinding.getBoundContext()) {
					this.getRouter().getTargets().display("objectNotFound");
					return;
				}

				var oResourceBundle = this.getResourceBundle(),
					oObject = oView.getBindingContext().getObject(),
					sObjectId = oObject.OrderID,
                    sObjectName = oObject.CustomerID,
                    oOrderItems = this.getView().byId("objectTable"),
                    aFilters = [new Filter(
                                        "OrderID", 
                                        FilterOperator.EQ, 
                                        sObjectId
                                )];

                // Order Table Columns
                var oColItems = new sap.m.ColumnListItem("colItems",{type:"Active"});
                oColItems.addCell(new sap.m.ObjectIdentifier("objectTableCellProductID",{
                    title: "{ProductID}"
                }));
                oColItems.addCell(new sap.m.Text("objectTableCellUnitPriceID",{
                    text: "{UnitPrice}"
                }));
                oColItems.addCell(new sap.m.Text("objectTableCellQuantity",{
                    text: "{Quantity}"
                }));
                oColItems.addCell(new sap.m.ObjectStatus("objectTableCellDiscount",{
                    text: "{Discount}",
                    state: {
                        path: "Discount",
                        formatter: this.formatter.statusDiscount
                    }
                }));

                // Order Table Items
                oOrderItems.bindAggregation("items","/Orders(" + oObject.OrderID + ")/Order_Details",oColItems);
                
                //Order Subtotal
                this._updateOrderSubtotal(oObject.OrderID);

				// Everything went fine.
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("saveAsTileTitle", [sObjectName]));
				oViewModel.setProperty("/shareOnJamTitle", sObjectName);
				oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
				oViewModel.setProperty("/shareSendEmailMessage",
                oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
            },

            _applyStyle : function(sId, sStyle){
                if(this.byId(sId)){
                    this.byId(sId).addStyleClass(sStyle);
                }
            },

            _updateOrderSubtotal : function(sObjectId) {
                var that = this;
                var fnSuccess = function(oData){
                    var sTotalValue = Number.parseFloat(oData.Subtotal).toFixed(0);
                    if(this.byId("objectTotalValue")){
                        this.byId("objectTotalValue").setNumber(sTotalValue);
                    }
                }.bind(that);

                var oModel = this.getView().getModel();
                var sPath = "/Order_Subtotals("+ sObjectId +")"
                oModel.read(sPath,{
                            success: fnSuccess
                });
            }

		});

	}
);