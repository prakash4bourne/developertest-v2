sap.ui.define([
		"au/com/bournedigital/developertestv2/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
        "au/com/bournedigital/developertestv2/model/formatter",
        'sap/ui/core/Fragment',
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, History, formatter, Fragment, Filter, FilterOperator) {
		"use strict";

		return BaseController.extend("au.com.bournedigital.developertestv2.controller.Worklist", {

			formatter: formatter,

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			/**
			 * Called when the worklist controller is instantiated.
			 * @public
			 */
			onInit : function () {
				var oViewModel,
					iOriginalBusyDelay,
					oTable = this.byId("worklistTable");

				// Put down worklist table's original value for busy indicator delay,
				// so it can be restored later on. Busy handling on the table is
				// taken care of by the table itself.
				iOriginalBusyDelay = oTable.getBusyIndicatorDelay();
				// keeps the search state
                this._aCustomerSearchData = [];
                this._aEmployeeSearchData = [];

				// Model used to manipulate control states
				oViewModel = new JSONModel({
					worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
					saveAsTileTitle: this.getResourceBundle().getText("saveAsTileTitle", this.getResourceBundle().getText("worklistViewTitle")),
					shareOnJamTitle: this.getResourceBundle().getText("worklistTitle"),
					shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
					shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
					tableNoDataText : this.getResourceBundle().getText("tableNoDataText"),
					tableBusyDelay : 0
				});
				this.setModel(oViewModel, "worklistView");

				// Make sure, busy indication is showing immediately so there is no
				// break after the busy indication for loading the view's meta data is
				// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
				oTable.attachEventOnce("updateFinished", function(){
					// Restore original busy indicator delay for worklist's table
					oViewModel.setProperty("/tableBusyDelay", iOriginalBusyDelay);
                });
                
			},

			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * Triggered by the table's 'updateFinished' event: after new table
			 * data is available, this handler method updates the table counter.
			 * This should only happen if the update was successful, which is
			 * why this handler is attached to 'updateFinished' and not to the
			 * table's list binding's 'dataReceived' method.
			 * @param {sap.ui.base.Event} oEvent the update finished event
			 * @public
			 */
			onUpdateFinished : function (oEvent) {
				// update the worklist's object counter after the table update
				var sTitle,
					oTable = oEvent.getSource(),
					iTotalItems = oEvent.getParameter("total");
				// only update the counter if the length is final and
				// the table is not empty
				if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
					sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
				} else {
					sTitle = this.getResourceBundle().getText("worklistTableTitle");
				}
				this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
			},

			/**
			 * Event handler when a table item gets pressed
			 * @param {sap.ui.base.Event} oEvent the table selectionChange event
			 * @public
			 */
			onPress : function (oEvent) {
				// The source is the list item that got pressed
				this._showObject(oEvent.getSource());
			},



			/**
			 * Event handler when the share in JAM button has been clicked
			 * @public
			 */
			onShareInJamPress : function () {
				var oViewModel = this.getModel("worklistView"),
					oShareDialog = sap.ui.getCore().createComponent({
						name: "sap.collaboration.components.fiori.sharing.dialog",
						settings: {
							object:{
								id: location.href,
								share: oViewModel.getProperty("/shareOnJamTitle")
							}
						}
					});
				oShareDialog.open();
			},

			onSearch : function (oEvent) {
				if (oEvent.getParameters().refreshButtonPressed) {
					// Search field's 'refresh' button has been pressed.
					// This is visible if you select any master list item.
					// In this case no new search is triggered, we only
					// refresh the list binding.
					this.onRefresh();
				} else {
                    
                    var aTableSearchState = [];
					var sQuery = oEvent.getParameter("query");

					if (sQuery && sQuery.length > 0) {
                        this._setSearchFilters(sQuery,"/Customers","CustomerID",false);
                        this._setSearchFilters(sQuery,"/Employees","EmployeeID",true);
                    }else{
                        this._applySearch(aTableSearchState);
                    }
				}

			},

			/**
			 * Event handler for refresh event. Keeps filter, sort
			 * and group settings and refreshes the list binding.
			 * @public
			 */
			onRefresh : function () {
				var oTable = this.byId("worklistTable");
				oTable.getBinding("items").refresh();
			},

            showEmployeeDetail: function(oEvent) {
                var oCtx = oEvent.getSource().getBindingContext(),
                    oControl = oEvent.getSource(),
                    oView = this.getView();

                // Create Employee Detail Popover
                if (!this._employeeDetailPopover) {
                    this._employeeDetailPopover = Fragment.load({
                        id: oView.getId(),
                        name: "au.com.bournedigital.developertestv2.view.fragment.EmployeeDetail",
                        controller: this
                    }).then(function (oPopover) {
                        oView.addDependent(oPopover);
                        oPopover.attachAfterOpen(function() {
                            this.disablePointerEvents();
                        }, this);
                        oPopover.attachAfterClose(function() {
                            this.enablePointerEvents();
                        }, this);
                        return oPopover;
                    }.bind(this));
                }
                this._employeeDetailPopover.then(function(oPopover) {
                    oPopover.bindElement(oCtx.getPath());
                    oPopover.openBy(oControl);
                });
            },
            closeEmployeeDetail: function () {
                this.byId("employeeDetailPopover").close();
            },
            disablePointerEvents: function () {
			    this.byId("worklistTable").getDomRef().style["pointer-events"] = "none";
            },

            enablePointerEvents: function () {
                this.byId("worklistTable").getDomRef().style["pointer-events"] = "auto";
            },
			/* =========================================================== */
			/* internal methods                                            */
			/* =========================================================== */

			/**
			 * Shows the selected item on the object page
			 * On phones a additional history entry is created
			 * @param {sap.m.ObjectListItem} oItem selected Item
			 * @private
			 */
			_showObject : function (oItem) {
				this.getRouter().navTo("object",{objectId:oItem.getBindingContext().getProperty("OrderID")});
			},

			/**
			 * Internal helper method to apply both filter and search state together on the list binding
			 * @param {sap.ui.model.Filter[]} aTableSearchState An array of filters for the search
			 * @private
			 */
			_applySearch: function(aTableSearchState) {
				var oTable = this.byId("worklistTable"),
					oViewModel = this.getModel("worklistView");
				oTable.getBinding("items").filter(aTableSearchState,"Application");
				// changes the noDataText of the list in case there are no filter results
				if (aTableSearchState.length !== 0) {
					oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
				}
            },
            
            _setSearchFilters: function(sQuery,sPath,sSelectColumns,bApply) {               
                var oModel = this.getView().getModel();
                var aReadFilters = [];
                var oLocalFilter = this._getReadFilters(sQuery,sPath);
                if(oLocalFilter){
                    aReadFilters.push(oLocalFilter);
                }

                var that = this;
                that._bApplyFilter = bApply;
                switch(sPath){
                    case "/Customers":
                        oModel.read(sPath,{
                            urlParameters: {
                                "$select": sSelectColumns
                            },
                            filters: aReadFilters,
                            success: function(oData, oResponse){
                                that._aCustomerSearchData = oData.results;
                            }.bind(that)
                        });
                        break;
                    case "/Employees":
                        oModel.read(sPath,{
                            urlParameters: {
                                "$select": sSelectColumns
                            },
                            filters: aReadFilters,
                            success: function(oData, oResponse){
                                that._aEmployeeSearchData = oData.results;
                                that._buildFilterAndApplySearch();
                            }.bind(that),
                            error: function(oError){
                                that._buildFilterAndApplySearch();
                            }.bind(that)
                        });
                        break;
                    default:
                }
            },
            _buildFilterAndApplySearch: function(){
                var aFilters = [];

                // Add CustomerFilters
                var aCustomerFilters = this._aCustomerSearchData.reduce(function (aResult, oControl) {

                    if (oControl.CustomerID) {
                        aResult.push(new Filter({
                            path: "CustomerID",
                            operator: FilterOperator.EQ,
                            value1: oControl.CustomerID
                        }));
                    }
					
					return aResult;
                }, []);
                if(aCustomerFilters.length > 0){
                    var oCustomerFilter = new Filter({
                        filters: aCustomerFilters,
                        and: false
                    })
                    aFilters.push(oCustomerFilter);
                }

                // Add Employee Filter
                var aEmployeeFilters = this._aEmployeeSearchData.reduce(function (aResult, oControl) {

                    if (oControl.EmployeeID) {
                        aResult.push(new Filter({
                            path: "EmployeeID",
                            operator: FilterOperator.EQ,
                            value1: oControl.EmployeeID
                        }));
                    }
					
					return aResult;
                }, []);

                if(aEmployeeFilters.length > 0){
                    var oEmployeeFilter = new Filter({
                        filters: aEmployeeFilters,
                        and: false
                    })
                    aFilters.push(oEmployeeFilter);
                }

                if(aFilters.length > 0) {
                    var aTableSearchState = new Filter( { 
                        filters: aFilters,
                        and: false
                    });

                    this._applySearch(aTableSearchState);
                }
            },

            _getReadFilters: function(sQuery,sPath) {
                var aRdFilters = [];
                switch(sPath) {
                    case "/Customers":
                        aRdFilters.push(new Filter({ path: "CompanyName", operator: FilterOperator.Contains, value1:sQuery}));
                        break;
                    case "/Employees":
                        aRdFilters.push(new Filter({ path: "FirstName", operator: FilterOperator.Contains, value1:sQuery}));
                        aRdFilters.push(new Filter({ path: "LastName", operator: FilterOperator.Contains, value1:sQuery}));
                        break;
                    default:
                }
                
                var oFilter = new Filter({
                    filters: aRdFilters,
                    and: false
                });
                return oFilter;
            },

		});
	}
);