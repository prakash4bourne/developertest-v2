sap.ui.define([
	] , function () {
		"use strict";

		return {

			/**
			 * Rounds the number unit value to 2 digits
			 * @public
			 * @param {string} sValue the number string to be rounded
			 * @returns {string} sValue with 2 digits rounded
			 */
			numberUnit : function (sValue) {
				if (!sValue) {
					return "";
				}
				return parseFloat(sValue).toFixed(2);
            },
            
            /**
             * Expects a Base64 string and trims the first 104 bytes from it and returns 
             * a Data Uri using the trimmed Base64 string.
             */ 
            trimSuperfluousBytes : function (sVal) {
                var sTrimmed;
                if(typeof sVal === "string"){
                    sTrimmed = sVal.substr(104);
                    return "data:image/bmp;base64," + sTrimmed;
                }                          
            },

            statusDiscount : function(sDiscount){
                var sStatus;
                sDiscount = sDiscount * 100;
                if(sDiscount > 40 ){
                    sSttaus = "Error";
                }if(sDiscount < 40 && sDiscount > 20){
                    sStatus = "Warning";
                }
                else{
                    sStatus = "Success";
                }
                return sStatus;
            },

            countryCurrency : function(sCountry){
                var sCurrency;
                switch(sCountry){
                    case "Germany" : sCurrency = "EUR"; break;
                    case "USA" : sCurrency = "USD"; break;
                    default : sCurrency = "AUD"
                }
                return sCurrency;
            }

		};

	}
);